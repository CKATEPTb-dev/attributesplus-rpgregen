package ru.ckateptb.attributesplus.attributesplusrpgregen;

import ru.ckateptb.attributesplus.attributesplus.objects.Attribute;
import ru.ckateptb.attributesplus.attributesplusrpgregen.config.Config;
import ru.ckateptb.tableapi.configs.Configurable;

public abstract class RPGRegenAttribute extends Attribute {

    @Override
    public String getRequiredPlugin() {
        return "RPGRegen";
    }

    @Override
    public Configurable getConfig() {
        return new Config(); //todo
    }
}