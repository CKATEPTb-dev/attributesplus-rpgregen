package ru.ckateptb.attributesplus.attributesplusrpgregen;

import me.gilles_m.rpgregen2.RPGRegen;
import me.gilles_m.rpgregen2.managers.RegenManager;
import me.gilles_m.rpgregen2.mechanics.RegenMechanic;
import me.gilles_m.rpgregen2.softDependencies.SkillAPIDependencies;
import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitTask;
import ru.ckateptb.attributesplus.attributesplus.objects.Attributed;
import ru.ckateptb.attributesplus.attributesplusrpgregen.config.Config;

import java.lang.reflect.Field;

/**
 * @link https://github.com/spigot-gillesm/RPGRegen
 */
public class HealingAttribute extends RPGRegenAttribute {
    private RPGRegen instance;

    @Override
    public Listener getListener() {
        try {
            Class clazz = RegenMechanic.class;
            Field field = clazz.getDeclaredField("task");
            field.setAccessible(true);
            BukkitTask task = (BukkitTask) field.get(RPGRegen.getRegenMechanic());
            task.cancel();
            task = Bukkit.getScheduler().runTaskTimer(instance = RPGRegen.getInstance(), () -> RegenMechanic.playersOutOfCombat.forEach(player -> {
                if (RegenManager.canRegen(player)) heal(player); //Redirect | LMAO ROFL HACKERMAN 228
            }), 0, Config.interval / 50);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @link me/gilles_m/rpgregen2/mechanics/RegenMechanic#heal(Lorg/bukkit/entity/Player;)
     */
    private void heal(Player player) {
        if (player.isDead()) return;
        FileConfiguration config = instance.getConfig();
        final float playerMaxHealth = (float) player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
        final float playerCurrentHealth = (float) player.getHealth();
        final float playerFoodLevel = (player.getFoodLevel());
        float finalRegenAmount = (float) config.getDouble("regen amount");
        final boolean useExperienceLevel = config.getBoolean("use-experience-level");
        final boolean useSkillAPILevel = config.getBoolean("use-SkillAPI-experience-level");
        final boolean useFoodLevel = config.getBoolean("use-food-level");
        final boolean spawnParticles = config.getBoolean("particles");
        if (!(useSkillAPILevel && useExperienceLevel)) {
            final int perExperience = config.getInt("per-level");
            final float regenBonus = (float) config.getDouble("regen-bonus");
            if (useExperienceLevel) {
                final int playerExp = player.getLevel();
                finalRegenAmount += (float) playerExp / perExperience * regenBonus;
            }
            if (useSkillAPILevel && instance.getSkillAPI() != null) {
                final int playerExp = SkillAPIDependencies.getSkillLevel(player);
                finalRegenAmount += (float) playerExp / perExperience * regenBonus;
            }
        }
        if (useFoodLevel) finalRegenAmount *= playerFoodLevel / 20;
        //AttributesPlus - start
        Attributed attributed = new Attributed(player);
        double amount = attributed.get(getDisplay());
        finalRegenAmount += amount;
        //AttributesPlus - end
        player.setHealth(Math.min(finalRegenAmount + playerCurrentHealth, playerMaxHealth));
        if (spawnParticles) RegenManager.playParticles(player);
    }

    @Override
    public String getName() {
        return "healing";
    }

    @Override
    public String getDisplay() {
        return Config.healing;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public Boolean isVariable() {
        return true;
    }
}