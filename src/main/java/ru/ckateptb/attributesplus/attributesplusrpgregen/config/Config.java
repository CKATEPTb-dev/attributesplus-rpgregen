package ru.ckateptb.attributesplus.attributesplusrpgregen.config;

import ru.ckateptb.attributesplus.attributesplus.AttributesPlus;
import ru.ckateptb.tableapi.configs.Configurable;

import java.io.File;

@Configurable.ConfigFile()
public class Config extends Configurable {
    @Configurable.ConfigField(name = "attributes.healing.display")
    public static String healing = "§b§lHealing: ";

    @Configurable.ConfigField(name = "attributes.healing.interval", comment = "in millisecond")
    public static Long interval = 10000L;

    public Config() {
        super(AttributesPlus.getInstance().getAttributesFolder() + File.separator + "RPGRegen");
    }
}